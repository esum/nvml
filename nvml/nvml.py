#!/usr/bin/python3

from enum import Enum
from functools import lru_cache, cached_property
import pynvml


class Brand(Enum):
	UNKNOWN = pynvml.NVML_BRAND_UNKNOWN
	QUADRO = pynvml.NVML_BRAND_QUADRO
	TESLA = pynvml.NVML_BRAND_TESLA
	NVS = pynvml.NVML_BRAND_NVS
	GRID = pynvml.NVML_BRAND_GRID
	GEFORCE = pynvml.NVML_BRAND_GEFORCE
	TITAN = pynvml.NVML_BRAND_TITAN
	NVIDIA_VAPPS = pynvml.NVML_BRAND_NVIDIA_VAPPS
	NVIDIA_VPC = pynvml.NVML_BRAND_NVIDIA_VPC
	NVIDIA_VCS = pynvml.NVML_BRAND_NVIDIA_VCS
	NVIDIA_VWS = pynvml.NVML_BRAND_NVIDIA_VWS
	NVIDIA_VGAMING = pynvml.NVML_BRAND_NVIDIA_VGAMING
	QUADRO_RTX = pynvml.NVML_BRAND_QUADRO_RTX
	NVIDIA_RTX = pynvml.NVML_BRAND_NVIDIA_RTX
	NVIDIA = pynvml.NVML_BRAND_NVIDIA
	GEFORCE_RTX = pynvml.NVML_BRAND_GEFORCE_RTX
	TITAN_RTX = pynvml.NVML_BRAND_TITAN_RTX

def brand_name(brand: Brand) -> str:
	"""Returns the name associated with the brand."""
	assert isinstance(brand, Brand)
	if brand == Brand.QUADRO:
		return 'Quadro'
	elif brand == Brand.TESLA:
		return 'Tesla'
	elif brand == Brand.NVS:
		return 'NVS'
	elif brand == Brand.GRID:
		return 'Grid'
	elif brand == Brand.GEFORCE:
		return 'GeForce'
	elif brand == Brand.TITAN:
		return 'Titan'
	elif brand == Brand.NVIDIA_VAPPS:
		return 'NVIDIA Virtual Applications'
	elif brand == Brand.NVIDIA_VPC:
		return 'NVIDIA Virtual PC'
	elif brand == Brand.NVIDIA_VCS:
		return 'NVIDIA Virtual Compute Server'
	elif brand == Brand.NVIDIA_VWS:
		return 'NVIDIA RTX Virtual Workstation'
	elif brand == Brand.NVIDIA_VGAMING:
		return 'NVIDIA vGaming'
	elif brand == Brand.QUADRO_RTX:
		return 'Quadro RTX'
	elif brand == Brand.NVIDIA_RTX:
		return 'NVIDIA RTX'
	elif brand == Brand.NVIDIA:
		return 'NVIDIA'
	elif brand == Brand.GEFORCE_RTX:
		return 'Geforce RTX'
	elif brand == Brand.TITAN_RTX:
		return 'Titan RTX'
	else:
		return 'Unknown'

class Pstate(Enum):
	P0 = pynvml.NVML_PSTATE_0
	P1 = pynvml.NVML_PSTATE_1
	P2 = pynvml.NVML_PSTATE_2
	P3 = pynvml.NVML_PSTATE_3
	P4 = pynvml.NVML_PSTATE_4
	P5 = pynvml.NVML_PSTATE_5
	P6 = pynvml.NVML_PSTATE_6
	P7 = pynvml.NVML_PSTATE_7
	P8 = pynvml.NVML_PSTATE_8
	P9 = pynvml.NVML_PSTATE_9
	P10 = pynvml.NVML_PSTATE_10
	P11 = pynvml.NVML_PSTATE_11
	P12 = pynvml.NVML_PSTATE_12
	P13 = pynvml.NVML_PSTATE_13
	P14 = pynvml.NVML_PSTATE_14
	P15 = pynvml.NVML_PSTATE_15
	UNKNOWN = pynvml.NVML_PSTATE_UNKNOWN

class Arch(Enum):
	KEPLER = pynvml.NVML_DEVICE_ARCH_KEPLER
	MAXWELL = pynvml.NVML_DEVICE_ARCH_MAXWELL
	PASCAL = pynvml.NVML_DEVICE_ARCH_PASCAL
	VOLTA = pynvml.NVML_DEVICE_ARCH_VOLTA
	TURING = pynvml.NVML_DEVICE_ARCH_TURING
	AMPERE = pynvml.NVML_DEVICE_ARCH_AMPERE
	UNKNOWN = pynvml.NVML_DEVICE_ARCH_UNKNOWN

def arch_name(arch: Arch):
	"""Returns the name of the architecture."""
	assert isinstance(arch, Arch)
	if arch == Arch.KEPLER:
		return 'Kepler'
	elif arch == Arch.MAXWELL:
		return 'Maxwell'
	elif arch == Arch.PASCAL:
		return 'Pascal'
	elif arch == Arch.VOLTA:
		return 'Volta'
	elif arch == Arch.TURING:
		return 'Turing'
	elif arch == Arch.AMPERE:
		return 'Ampere'
	else:
		return 'Unknown'

class Gpu:
	def __init__(self, handle):
		self.handle = handle

	@cached_property
	def architecture(self) -> Arch:
		"""Get architecture for device."""
		return Arch(pynvml.nvmlDeviceGetArchitecture(self.handle))

	@cached_property
	def board_id(self) -> int:
		"""Retrieves the device boardId."""
		return pynvml.nvmlDeviceGetBoardId(self.handle)

	@cached_property
	def board_part_number(self) -> str:
		"""Retrieves the the device board part number which is programmed into the board's InfoROM."""
		return pynvml.nvmlDeviceGetBoardPartNumber(self.handle).decode('ascii')

	@cached_property
	def brand(self) -> Brand:
		"""Retrieves the brand of this device."""
		return Brand(pynvml.nvmlDeviceGetBrand(self.handle))

	@cached_property
	def minor_number(self) -> int:
		"""
		Retrieves minor number for the device.
		The minor number for the device is such that the Nvidia device node file for each GPU will have the form /dev/nvidia[minor number].
		"""
		return pynvml.nvmlDeviceGetMinorNumber(self.handle)

	@cached_property
	def name(self) -> str:
		"""Retrieves the name of this device."""
		return pynvml.nvmlDeviceGetName(self.handle).decode('ascii')

	@cached_property
	def pci_info(self) -> pynvml.nvmlPciInfo_t:
		"""Retrieves the PCI attributes of this device."""
		return pynvml.nvmlDeviceGetPciInfo_v3(self.handle)

	@cached_property
	def serial(self) -> str:
		"""Retrieves the globally unique board serial number associated with this device's board."""
		return pynvml.nvmlDeviceGetSerial(self.handle).decode('ascii')

	@cached_property
	def uuid(self) -> str:
		"""Retrieves the globally unique immutable UUID associated with this device, as a 5 part hexadecimal string, that augments the immutable, board serial identifier."""
		return pynvml.nvmlDeviceGetUUID(self.handle).decode('ascii')

	@property
	def fan_speed(self) -> int:
		"""Retrieves the intended operating speed of the device's fan."""
		return pynvml.nvmlDeviceGetFanSpeed(self.handle)

	@property
	def memory_info(self) -> pynvml.c_nvmlMemory_t:
		"""Retrieves the amount of used, free and total memory available on the device, in bytes."""
		return pynvml.nvmlDeviceGetMemoryInfo(self.handle)

	@property
	def pcie_throughput_tx(self) -> int:
		"""
		Retrieves PCIe utilization information.
		This function is querying a byte counter over a 20ms interval and thus is the PCIe throughput over that interval.
		"""
		return pynvml.nvmlDeviceGetPcieThroughput(self.handle, pynvml.NVML_PCIE_UTIL_TX_BYTES)

	@property
	def pcie_throughput_rx(self) -> int:
		"""
		Retrieves PCIe utilization information.
		This function is querying a byte counter over a 20ms interval and thus is the PCIe throughput over that interval.
		"""
		return pynvml.nvmlDeviceGetPcieThroughput(self.handle, pynvml.NVML_PCIE_UTIL_RX_BYTES)

	@property
	def performance_state(self) -> Pstate:
		"""Retrieves the current performance state for the device."""
		return Pstate(pynvml.nvmlDeviceGetPerformanceState(self.handle))

	@property
	def power_usage(self) -> int:
		"""Retrieves power usage for this GPU in milliwatts and its associated circuitry (e.g. memory)."""
		return pynvml.nvmlDeviceGetPowerUsage(self.handle)

	@property
	def temperature(self) -> int:
		"""Retrieves the current temperature readings for the device, in degrees C."""
		return pynvml.nvmlDeviceGetTemperature(self.handle, pynvml.NVML_TEMPERATURE_GPU)

	@property
	def utilization_rates(self) -> pynvml.c_nvmlUtilization_t:
		"""Retrieves the current utilization rates for the device's major subsystems."""
		return pynvml.nvmlDeviceGetUtilizationRates(self.handle)

	@property
	def vbios_version(self) -> str:
		"""Get VBIOS version of the device."""
		return pynvml.nvmlDeviceGetVbiosVersion(self.handle).decode('ascii')

	def __repr__(self) -> str:
		return("<{} {!r} {!r}>".format(self.__class__.__name__, self.name, self.uuid))

class GpuIterator:
	def __init__(self, context):
		self.current_device = 0
		self.context = context

	def __iter__(self):
		return self

	def __next__(self) -> Gpu:
		if self.current_device >= self.context.device_count:
			raise StopIteration
		ret = self.context[self.current_device]
		self.current_device += 1
		return ret

class Nvml:
	def __init__(self):
		pynvml.nvmlInit()
		self.initialized = True

	def shutdown(self):
		if self.initialized:
			pynvml.nvmlShutdown()
			self.initialized = False

	def __enter__(self):
		return self

	def __exit__(self, exc_type, exc_value, traceback):
		self.shutdown()

	def __del__(self):
		self.shutdown()

	@cached_property
	def device_count(self) -> int:
		"""Retrieves the number of compute devices in the system. A compute device is a single GPU."""
		return pynvml.nvmlDeviceGetCount()

	def __len__(self):
		return self.device_count

	@lru_cache(maxsize=None)
	def __getitem__(self, other: int):
		"""Returns a Gpu object corresponding to the argument."""
		assert isinstance(other, int)
		assert 0 <= other < self.device_count
		return Gpu(pynvml.nvmlDeviceGetHandleByIndex(other))

	def __iter__(self) -> GpuIterator:
		return GpuIterator(self)

	@property
	def driver_version(self) -> str:
		"""Retrieves the version of the system's graphics driver."""
		return pynvml.nvmlSystemGetDriverVersion().decode('ascii')

	@property
	def cuda_driver_version(self) -> int:
		"""Retrieves the version of the CUDA driver from the shared library."""
		return pynvml.nvmlSystemGetCudaDriverVersion()

	@cached_property
	def nvml_version(self) -> str:
		"""Retrieves the version of the NVML library."""
		return pynvml.nvmlSystemGetNVMLVersion().decode('ascii')
