# NVML

This library provides a high level interface for pynvml.

# Examples

Print the names of all GPUs:

```python
from nvml import Nvml

with Nvml() as nvml:
    for gpu in nvml:
        print(gpu.name)
```

Print some statistics about the first GPU of the system:

```python
from nvml import Nvml

nvml = Nvml()
gpu = nvml[0]
print('Fan speed: ', gpu.fan_speed, '%')
print('Temperature: ', gpu.temperature, 'C')
```
